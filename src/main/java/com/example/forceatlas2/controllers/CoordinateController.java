package com.example.forceatlas2.controllers;

import com.example.forceatlas2.models.responses.NetworkxLayoutResponse;
import com.example.forceatlas2.services.CoordinateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/coordinate")
public class CoordinateController {

    private final CoordinateService coordinateService;

    @Autowired
    public CoordinateController(CoordinateService coordinateService) {
        this.coordinateService = coordinateService;
    }

    @GetMapping("/get")
    public NetworkxLayoutResponse getGraphCoordinates(@RequestParam("file")MultipartFile file) {
        return coordinateService.getGraphCoordinates(file);
    }
}
