package com.example.forceatlas2.services;

import com.example.forceatlas2.Forceatlas2Application;
import com.example.forceatlas2.exceptions.GraphException;
import com.example.forceatlas2.models.Edge;
import com.example.forceatlas2.models.ForceAtlas2;
import com.example.forceatlas2.models.responses.NetworkxLayoutResponse;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVReader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = GraphException.class)
public class CoordinateService {

    public static final Gson GSON = new GsonBuilder().create();
    private static final Logger LOGGER = LoggerFactory.getLogger(CoordinateService.class);

    @Autowired
    public CoordinateService() {}

    public NetworkxLayoutResponse getGraphCoordinates(MultipartFile file) {
        CSVReader reader = null;
        String[] line;
        List<Edge> edges = new ArrayList<Edge>();
        try {
            File convertFile = convert(file);
            reader = new CSVReader(new FileReader(convertFile));

            while ((line = reader.readNext()) != null) {
                if(StringUtils.isEmpty(line[0]) || StringUtils.isEmpty(line[1])){
                    continue;
                }
                System.out.println("source = " + line[0] + ", target = " + line[1] + ";");
                Edge edge = new Edge();
                edge.setSource(new BigInteger(line[0]));
                edge.setTarget(new BigInteger(line[1]));
                edges.add(edge);
            }

//            LOGGER.info(GSON.toJson(edges));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ForceAtlas2 forceAtlas2 = new ForceAtlas2(true, false,
                1.0, 1.0,
                true, 1.2, 2.0,
                false, 1.0, true);

        NetworkxLayoutResponse positions = forceAtlas2.forceAtlas2NetworkxLayout(edges, 10);
        LOGGER.info(GSON.toJson(positions));
        return positions;
    }

    public static File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

}
