package com.example.forceatlas2.models;

import com.example.forceatlas2.models.responses.SpeedAndSpeedEfficiencyResponse;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ForceAtlas2Util {

    public static class Region {

        private double mass;
        private double massCenterX;
        private double massCenterY;
        private double size;
        private final List<Node> nodes;
        private final List<Region> subregions = new ArrayList<>();

        public Region(Node[] nodes) {
            this.nodes = new ArrayList<>();
            this.nodes.addAll(Arrays.asList(nodes));
            updateMassAndGeometry();
        }

        public Region(List<Node> nodes) {
            this.nodes = new ArrayList<>(nodes);
            updateMassAndGeometry();
        }

        private void updateMassAndGeometry() {
            if (nodes.size() > 1) {
                // Compute Mass
                mass = 0;
                double massSumX = 0;
                double massSumY = 0;
                for (Node n : nodes) {
                    mass += n.mass;
                    massSumX += n.x * n.mass;
                    massSumY += n.y * n.mass;
                }
                massCenterX = massSumX / mass;
                massCenterY = massSumY / mass;

                // Compute size
                size = Double.MIN_VALUE;
                for (Node n : nodes) {
                    double distance = Math.sqrt((n.x - massCenterX) * (n.x - massCenterX) + (n.y - massCenterY) * (n.y - massCenterY));
                    size = Math.max(size, 2 * distance);
                }
            }
        }

        public synchronized void buildSubRegions() {
            if (nodes.size() > 1) {
                ArrayList<Node> leftNodes = new ArrayList<>();
                ArrayList<Node> rightNodes = new ArrayList<>();
                for (Node n : nodes) {
                    ArrayList<Node> nodesColumn = (n.x < massCenterX) ? (leftNodes) : (rightNodes);
                    nodesColumn.add(n);
                }

                ArrayList<Node> topleftNodes = new ArrayList<>();
                ArrayList<Node> bottomleftNodes = new ArrayList<>();
                for (Node n : leftNodes) {
                    ArrayList<Node> nodesLine = (n.y < massCenterY) ? (topleftNodes) : (bottomleftNodes);
                    nodesLine.add(n);
                }

                ArrayList<Node> bottomrightNodes = new ArrayList<>();
                ArrayList<Node> toprightNodes = new ArrayList<>();
                for (Node n : rightNodes) {
                    ArrayList<Node> nodesLine = (n.y < massCenterY) ? (toprightNodes) : (bottomrightNodes);
                    nodesLine.add(n);
                }

                if (topleftNodes.size() > 0) {
                    if (topleftNodes.size() < nodes.size()) {
                        Region subregion = new Region(topleftNodes);
                        subregions.add(subregion);
                    } else {
                        for (Node n : topleftNodes) {
                            ArrayList<Node> oneNodeList = new ArrayList<>();
                            oneNodeList.add(n);
                            Region subregion = new Region(oneNodeList);
                            subregions.add(subregion);
                        }
                    }
                }
                if (bottomleftNodes.size() > 0) {
                    if (bottomleftNodes.size() < nodes.size()) {
                        Region subregion = new Region(bottomleftNodes);
                        subregions.add(subregion);
                    } else {
                        for (Node n : bottomleftNodes) {
                            ArrayList<Node> oneNodeList = new ArrayList<>();
                            oneNodeList.add(n);
                            Region subregion = new Region(oneNodeList);
                            subregions.add(subregion);
                        }
                    }
                }
                if (bottomrightNodes.size() > 0) {
                    if (bottomrightNodes.size() < nodes.size()) {
                        Region subregion = new Region(bottomrightNodes);
                        subregions.add(subregion);
                    } else {
                        for (Node n : bottomrightNodes) {
                            ArrayList<Node> oneNodeList = new ArrayList<>();
                            oneNodeList.add(n);
                            Region subregion = new Region(oneNodeList);
                            subregions.add(subregion);
                        }
                    }
                }
                if (toprightNodes.size() > 0) {
                    if (toprightNodes.size() < nodes.size()) {
                        Region subregion = new Region(toprightNodes);
                        subregions.add(subregion);
                    } else {
                        for (Node n : toprightNodes) {
                            ArrayList<Node> oneNodeList = new ArrayList<>();
                            oneNodeList.add(n);
                            Region subregion = new Region(oneNodeList);
                            subregions.add(subregion);
                        }
                    }
                }

                for (Region subregion : subregions) {
                    subregion.buildSubRegions();
                }
            }
        }

        public void applyForce(Node n, double theta) {
            int coefficent = 0;
            boolean adjustSizes = false;
            if (nodes.size() < 2) {
                linRepulsion(n, nodes.get(0), 0, false);
            } else {
                double distance = Math.sqrt((n.x - massCenterX) * (n.x - massCenterX) + (n.y - massCenterY) * (n.y - massCenterY));
                if (distance * theta > size) {
                    linRepulsion_region(n, this, 0, false);
                } else {
                    for (Region subregion : subregions) {
                        subregion.applyForce(n, theta);
                    }
                }
            }
        }

        public void applyForceOnNodes(List<Node> nodes, double theta) {
            int coefficent = 0;
            boolean adjustSizes = false;
            for(Node n : nodes) {
                this.applyForce(n, theta);
            }
        }

        public double getMass() {
            return mass;
        }

        public void setMass(double mass) {
            this.mass = mass;
        }

        public double getMassCenterX() {
            return massCenterX;
        }

        public void setMassCenterX(double massCenterX) {
            this.massCenterX = massCenterX;
        }

        public double getMassCenterY() {
            return massCenterY;
        }

        public void setMassCenterY(double massCenterY) {
            this.massCenterY = massCenterY;
        }
    }

    // Repulsion function.  `n1` and `n2` should be nodes.  This will
    // adjust the dx and dy values of `n1`  `n2`
    public static void linRepulsion(Node n1, Node n2, double coefficent, boolean adjustSizes) {

        double xDist = n1.x - n2.x;
        double yDist = n1.y - n2.y;
        double distance2 = xDist * xDist + yDist * yDist;

        if (distance2 > 0) {
            // NB: factor = force / distance
            double factor = coefficent * n1.mass * n2.mass / distance2;

            n1.dx += xDist * factor;
            n1.dy += yDist * factor;

            n2.dx -= xDist * factor;
            n2.dy -= yDist * factor;
        }
    }

    public static void linRepulsion_region(Node n, Region r, int coefficent, boolean adjustSizes) {
        // Get the distance
        double xDist = n.x - r.getMassCenterX();
        double yDist = n.y- r.getMassCenterY();
        double distance = xDist * xDist + yDist * yDist;

        if (distance > 0) {
            // NB: factor = force / distance
            double factor = coefficent * n.mass * r.getMass() / distance;

            n.dx += xDist * factor;
            n.dy += yDist * factor;
        }
    }

    // Gravity repulsion function.  For some reason, gravity was included
    // within the linRepulsion function in the original gephi java code,
    // which doesn't make any sense (considering a. gravity is unrelated to
    // nodes repelling each other, and b. gravity is actually an
    // attraction)

    public static void linGravity(Node n, double g) {
        // Get the distance
        double xDist = n.x;
        double yDist = n.y;
        double distance = (float) Math.sqrt(xDist * xDist + yDist * yDist);

        if (distance > 0) {
            // NB: factor = force / distance
            double factor = n.mass * g / distance;

            n.dx -= xDist * factor;
            n.dy -= yDist * factor;
        }
    }

    // Strong gravity force function. `n` should be a node, and `g`
    // should be a constant by which to apply the force.
    public static void strongGravity(Node n, double g) {
        int coefficent = 0;
        double xDist = n.x;
        double yDist = n.y;

        if(xDist != 0 && yDist != 0) {
            double factor = coefficent * n.mass * g;
            n.dx -= xDist * factor;
            n.dy -= yDist * factor;
        }
    }

    // Attraction function.  `n1` and `n2` should be nodes.  This will
    // adjust the dx and dy values of `n1` and `n2`.  It does
    // not return anything.
    public static void linAttraction(Node n1, Node n2, double e, boolean distributedAttraction, double coefficent, boolean adjustSizes) {
        // Get the distance
        double xDist = n1.x - n2.x;
        double yDist = n1.y - n2.y;
        double factor;
        if(adjustSizes) {
            double distance = Math.sqrt(xDist * xDist + yDist * yDist) -n1.size - n2.size;

            if(distance > 0) {
                factor = -coefficent * e;

                n1.dx += xDist * factor;
                n1.dy += yDist * factor;

                n2.dx -= xDist * factor;
                n2.dy -= yDist * factor;
            }
        } else {
            if(!distributedAttraction) {
                factor = -coefficent * e;
            } else {
                factor = -coefficent * e / n1.mass;
            }
            n1.dx += xDist * factor;
            n1.dy += yDist * factor;
            n2.dx -= xDist * factor;
            n2.dy -= yDist * factor;
        }
    }

    public static void applyRepulsion(List<Node> nodes, double coefficent, boolean adjustSizes) {
        adjustSizes = false;
        int i = 0;
        for (Node n1 : nodes) {
            int j = i;
            for(Node n2 : nodes) {
                if(j == 0 ) {
                    break;
                }
                linRepulsion(n1, n2, coefficent, adjustSizes);
                j -= 1;
            }
            i += 1;
        }
    }

    public static void applyGravity(List<Node> nodes, double gravity, boolean useStrongGravity) {
        useStrongGravity = false;
        if(!useStrongGravity) {
            for (Node n : nodes) {
                linGravity(n, gravity);
            }
        } else {
            for (Node n : nodes) {
                strongGravity(n, gravity);
            }
        }
    }

    public static void applyAttraction(List<Node> nodes, List<Edge> edges, boolean distributedAttraction, double coefficent,
                                       double edgeWeightInfluence, boolean adjustSizes) {
        // Optimization, since usually edgeWeightInfluence is 0 or 1, and pow is slow
        if(edgeWeightInfluence == 0) {
            for(Edge e : edges) {
                linAttraction(nodes.get(e.source.intValue()), nodes.get(e.target.intValue()), 1, distributedAttraction, coefficent,
                        adjustSizes);
            }
        } else if (edgeWeightInfluence == 1) {
            System.out.println("Edges size: "+edges.size());
            System.out.println("Nodes size: "+nodes.size());

            for(Edge e : edges) {
//                linAttraction(nodes.get(e.source.intValue()), nodes.get(e.target.intValue()), e.weight, distributedAttraction, coefficent,
//                        adjustSizes);
                linAttraction(nodes.get(getEdgeIndexByValue(edges, e)),
                        nodes.get(getEdgeIndexByValue(edges, e)), e.weight, distributedAttraction,
                        coefficent, adjustSizes);
            }
//            for(int i = 0; i < edges.size(); i++) {
//                linAttraction(nodes.get(edges.get(i).source.intValue()), nodes.get(edges.get(i).target.intValue()),
//                        edges.get(i).weight, distributedAttraction, coefficent, adjustSizes);
//            }
        } else {
            for(Edge e : edges) {
                linAttraction(nodes.get(e.source.intValue()), nodes.get(e.target.intValue()), Math.pow(e.weight, edgeWeightInfluence),
                        distributedAttraction, coefficent, adjustSizes);
            }
        }
    }

    public static int getEdgeIndexByValue(List<Edge> edges, Edge edge) {
        return edges.indexOf(edge);
    }

    // Adjust speed and apply forces step
    public static SpeedAndSpeedEfficiencyResponse adjustSpeedAndApplyForces(List<Node> nodes, double speed, double speedEfficiency,
                                                                     double jitterTolerance, boolean adjustSizes) {
        adjustSizes = false;

        double totalSwinging = 0d;  // How much irregular movement
        double totalEffectiveTraction = 0d;  // Hom much useful movement
        for (Node n : nodes) {
            double swinging = Math.sqrt(Math.pow(n.oldDx - n.dx, 2) + Math.pow(n.oldDy - n.dy, 2));
            totalSwinging += n.mass * swinging;   // If the node has a burst change of direction, then it's not converging.
            totalEffectiveTraction += n.mass * 0.5 * Math.sqrt(Math.pow(n.oldDx + n.dx, 2) + Math.pow(n.oldDy + n.dy, 2));

        }
        // Optimize jitter tolerance
        // The 'right' jitter tolerance for this network. Bigger networks need more tolerance. Denser networks need less tolerance. Totally empiric.
        double estimatedOptimalJitterTolerance = 0.05 * Math.sqrt(nodes.size());
        double minJT = Math.sqrt(estimatedOptimalJitterTolerance);
        double maxJT = 10;
        double jt = jitterTolerance * Math.max(minJT, Math.min(maxJT, estimatedOptimalJitterTolerance * totalEffectiveTraction / Math.pow(nodes.size(), 2)));

        double minSpeedEfficiency = 0.05;

        // Protection against erratic behavior
        if (totalSwinging / totalEffectiveTraction > 2.0) {
            if (speedEfficiency > minSpeedEfficiency) {
                speedEfficiency *= 0.5;
            }
            jt = Math.max(jt, jitterTolerance);
        }

        double targetSpeed = jt * speedEfficiency * totalEffectiveTraction / totalSwinging;

        // Speed efficiency is how the speed really corresponds to the swinging vs. convergence tradeoff
        // We adjust it slowly and carefully
        if (totalSwinging > jt * totalEffectiveTraction) {
            if (speedEfficiency > minSpeedEfficiency) {
                speedEfficiency *= 0.7;
            }
        } else if (speed < 1000) {
            speedEfficiency *= 1.3;
        }

        // But the speed shoudn't rise too much too quickly, since it would make the convergence drop dramatically.
        double maxRise = 0.5;   // Max rise: 50%
        speed = speed + Math.min(targetSpeed - speed, maxRise * speed);

        // Apply forces
        if (adjustSizes) {
            // If nodes overlap prevention is active, it's not possible to trust the swinging mesure.
            for (Node n : nodes) {

                // Adaptive auto-speed: the speed of each node is lowered
                // when the node swings.
                double swinging = n.mass * Math.sqrt((n.oldDx - n.dx) * (n.oldDx - n.dx) + (n.oldDy - n.dy) * (n.oldDy - n.dy));
                double factor = 0.1 * speed / (1f + Math.sqrt(speed * swinging));

                double df = Math.sqrt(Math.pow(n.dx, 2) + Math.pow(n.dy, 2));
                factor = Math.min(factor * df, 10.) / df;

                double x = n.x + n.dx * factor;
                double y = n.y + n.dy * factor;

                n.x = (float) x;
                n.y = (float) y;

            }
        } else {
            for (Node n : nodes) {

                // Adaptive auto-speed: the speed of each node is lowered
                // when the node swings.
                double swinging = n.mass * Math.sqrt((n.oldDx - n.dx) * (n.oldDx - n.dx) + (n.oldDy - n.dy) * (n.oldDy - n.dy));
                //double factor = speed / (1f + Math.sqrt(speed * swinging));
                double factor = speed / (1f + Math.sqrt(speed * swinging));

                double x = n.x + n.dx * factor;
                double y = n.y + n.dy * factor;

                n.x = (float) x;
                n.y = (float) y;

            }
        }

        return new SpeedAndSpeedEfficiencyResponse(speed, speedEfficiency);

    }


}
