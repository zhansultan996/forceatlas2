package com.example.forceatlas2.models.responses;

import com.example.forceatlas2.models.Edge;
import com.example.forceatlas2.models.Node;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class NodeCoordinatesResponse {

    String graph = null;
    List<Edge> edges;
    List<Node> nodes;
    boolean directed = false;
    boolean multigraph = false;
}
