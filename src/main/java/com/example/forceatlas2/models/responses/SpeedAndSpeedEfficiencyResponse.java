package com.example.forceatlas2.models.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SpeedAndSpeedEfficiencyResponse {

    Double speed;
    Double speedEfficiency;
}
