package com.example.forceatlas2.models.responses;

import com.example.forceatlas2.models.Edge;
import com.example.forceatlas2.models.Node;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class NetworkxLayoutResponse {
    String graph = null;
    List<Edge> edges;
    List<Node> nodes;
    boolean directed = false;
    boolean multigraph = false;

    public NetworkxLayoutResponse(NodeCoordinatesResponse forceAtlas2Coordinates) {
        this.graph = forceAtlas2Coordinates.graph;
        this.edges = forceAtlas2Coordinates.edges;
        this.nodes = forceAtlas2Coordinates.nodes;
        this.directed = forceAtlas2Coordinates.directed;
        this.multigraph = forceAtlas2Coordinates.multigraph;

    }
}
