package com.example.forceatlas2.models;

import com.example.forceatlas2.models.responses.NetworkxLayoutResponse;
import com.example.forceatlas2.models.responses.NodeCoordinatesResponse;
import com.example.forceatlas2.models.responses.SpeedAndSpeedEfficiencyResponse;
import com.example.forceatlas2.services.CoordinateService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ForceAtlas2 {

    public static final Gson GSON = new GsonBuilder().create();
    private static final Logger LOGGER = LoggerFactory.getLogger(ForceAtlas2.class);

    private boolean outboundAttractionDistribution;
    private final boolean linLogMode = false;
    private boolean adjustSizes;
    private double edgeWeightInfluence;
    private double jitterTolerance;
    private boolean barnesHutOptimize;
    private double barnesHutTheta;
    private final boolean multiThreaded = false;
    private double scalingRatio;
    private boolean strongGravityMode;
    private double gravity;
    private boolean verbose;

    public ForceAtlas2(boolean outboundAttractionDistribution, boolean adjustSizes, double edgeWeightInfluence, double jitterTolerance, boolean barnesHutOptimize, double barnesHutTheta, double scalingRatio, boolean strongGravityMode, double gravity, boolean verbose) {
        this.outboundAttractionDistribution = outboundAttractionDistribution;
        this.adjustSizes = adjustSizes;
        this.edgeWeightInfluence = edgeWeightInfluence;
        this.jitterTolerance = jitterTolerance;
        this.barnesHutOptimize = barnesHutOptimize;
        this.barnesHutTheta = barnesHutTheta;
        this.scalingRatio = scalingRatio;
        this.strongGravityMode = strongGravityMode;
        this.gravity = gravity;
        this.verbose = verbose;
    }

    public boolean isOutboundAttractionDistribution() {
        return outboundAttractionDistribution;
    }

    public void setOutboundAttractionDistribution(boolean outboundAttractionDistribution) {
        this.outboundAttractionDistribution = outboundAttractionDistribution;
    }

    public boolean isAdjustSizes() {
        return adjustSizes;
    }

    public void setAdjustSizes(boolean adjustSizes) {
        this.adjustSizes = adjustSizes;
    }

    public double getEdgeWeightInfluence() {
        return edgeWeightInfluence;
    }

    public void setEdgeWeightInfluence(double edgeWeightInfluence) {
        this.edgeWeightInfluence = edgeWeightInfluence;
    }

    public double getJitterTolerance() {
        return jitterTolerance;
    }

    public void setJitterTolerance(double jitterTolerance) {
        this.jitterTolerance = jitterTolerance;
    }

    public boolean isBarnesHutOptimize() {
        return barnesHutOptimize;
    }

    public void setBarnesHutOptimize(boolean barnesHutOptimize) {
        this.barnesHutOptimize = barnesHutOptimize;
    }

    public double getBarnesHutTheta() {
        return barnesHutTheta;
    }

    public void setBarnesHutTheta(double barnesHutTheta) {
        this.barnesHutTheta = barnesHutTheta;
    }

    public double getScalingRatio() {
        return scalingRatio;
    }

    public void setScalingRatio(double scalingRatio) {
        this.scalingRatio = scalingRatio;
    }

    public boolean isStrongGravityMode() {
        return strongGravityMode;
    }

    public void setStrongGravityMode(boolean strongGravityMode) {
        this.strongGravityMode = strongGravityMode;
    }

    public double getGravity() {
        return gravity;
    }

    public void setGravity(double gravity) {
        this.gravity = gravity;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public Graph init(List<Edge> edges) {
        // Put nodes into a data structure we can understand
        boolean isParse = false;
        List<Node> nodes = new ArrayList<Node>();

        for(Integer i = 0; i < edges.size(); i++) {
            Node n = new Node(60);
            n.mass = 1; // todo: нужно завершить логику рассчета тяжести. файл forceatlas2.py#111
            n.oldDx = 0;
            n.oldDy = 0;
            n.dx = 0;
            n.dy = 0;
            n.x = Math.random() < 0.5 ? ((1-Math.random()) * (1-0) + 0) : (Math.random() * (1-0) + 0); // Random rd = new Random(); rd.nextFloat();
            n.y = Math.random() < 0.5 ? ((1-Math.random()) * (1-0) + 0) : (Math.random() * (1-0) + 0); // Random rd = new Random(); rd.nextFloat();
            n.name = i.toString();
            nodes.add(n);
        }

        return new Graph(nodes, edges);
    }


    public NodeCoordinatesResponse forceAtlas2(List<Edge> edges, int iterations) {
        double speed = 1.0;
        double speedEfficiency = 1.0;

        List<Node> nodes = init(edges).nodes;
        LOGGER.info(GSON.toJson(nodes));

        double outboundAttCompensation = 1.0;

        if(this.outboundAttractionDistribution) {
            double sum = 0;
            for (Node n : nodes) {
                sum += n.mass;
            }
            outboundAttCompensation = sum / nodes.size();
        }

        Timer barneshutTimer = new Timer("BarnesHut Approximation");
        Timer repulsionTimer = new Timer("Repulsion forces");
        Timer gravityTimer = new Timer("Gravitational forces");
        Timer attractionTimer = new Timer("Attraction forces");
        Timer applyforcesTimer = new Timer("AdjustSpeedAndApplyForces step");


        for(int i = 0; i < iterations; i++) {
            for(Node n : nodes) {
                n.oldDx = n.dx;
                n.oldDy = n.dy;
                n.dx = 0;
                n.dy = 0;
            }

            ForceAtlas2Util.Region rootRegion = new ForceAtlas2Util.Region(nodes);
            if(this.barnesHutOptimize) {
                barneshutTimer.start();
                rootRegion.buildSubRegions();
                barneshutTimer.stop();
            }

            repulsionTimer.start();
            if (barnesHutOptimize) {
                rootRegion.applyForceOnNodes(nodes, this.barnesHutTheta);
            } else {
                ForceAtlas2Util.applyRepulsion(nodes, this.scalingRatio, this.adjustSizes);
            }
            repulsionTimer.stop();

            gravityTimer.start();
            ForceAtlas2Util.applyGravity(nodes, this.gravity, this.strongGravityMode);
            gravityTimer.stop();

            attractionTimer.start();
            ForceAtlas2Util.applyAttraction(nodes, edges, this.outboundAttractionDistribution, this.scalingRatio,
                    this.edgeWeightInfluence, this.adjustSizes);
            attractionTimer.stop();

            applyforcesTimer.start();
            SpeedAndSpeedEfficiencyResponse speedAndSpeedEfficiency = ForceAtlas2Util.adjustSpeedAndApplyForces(nodes,
                    speed, speedEfficiency, this.jitterTolerance, this.adjustSizes);
            speed = speedAndSpeedEfficiency.getSpeed();
            speedEfficiency = speedAndSpeedEfficiency.getSpeedEfficiency();
            applyforcesTimer.stop();

            if(speedEfficiency < 0.05) {
                break;
            }
        }

        if(this.verbose) {
            if(this.barnesHutOptimize) {
                barneshutTimer.display();
            }
            repulsionTimer.display();
            gravityTimer.display();
            attractionTimer.display();
            applyforcesTimer.display();
        }

        NodeCoordinatesResponse response = new NodeCoordinatesResponse();

//        for(Node n : nodes) {
//            NodeCoordinatesResponse response = new NodeCoordinatesResponse();
//            response.setNodes(n);
//            response.setX(n.x);
//            response.setY(n.y);
//            response.add(response);
//        }

        response.setNodes(nodes);
        response.setEdges(edges);

        return response;
    }

    public NetworkxLayoutResponse forceAtlas2NetworkxLayout(List<Edge> edges, int iterations) {
        NodeCoordinatesResponse forceAtlas2Coordinates = this.forceAtlas2(edges, iterations);
        NetworkxLayoutResponse response = new NetworkxLayoutResponse(forceAtlas2Coordinates);
//        for(NodeCoordinatesResponse coord : l) {
//            NetworkxLayoutResponse res = new NetworkxLayoutResponse();
//            res.setNode(coord.getNode());
//            res.setX(coord.getX());
//            res.setY(coord.getY());
//
//            response.add(res);
//        }

        return response;
    }


}
