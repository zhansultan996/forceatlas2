package com.example.forceatlas2.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
public class Graph {

    List<Node> nodes;
    List<Edge> edges;
}
