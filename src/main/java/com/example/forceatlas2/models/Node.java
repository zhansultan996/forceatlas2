package com.example.forceatlas2.models;

public class Node {
    public double mass = 0.0; // score
    public double oldDx = 0.0;
    public double oldDy = 0.0;
    public double dx = 0.0;
    public double dy = 0.0;
    public double x = 0.0;
    public double y = 0.0;
    public int size;
    public String name; // id
    public String type = "circle";
    public double score;
    public String id = this.name;

    public Node(int size) {
        this.size = size;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public double getOldDx() {
        return oldDx;
    }

    public void setOldDx(double oldDx) {
        this.oldDx = oldDx;
    }

    public double getOldDy() {
        return oldDy;
    }

    public void setOldDy(double oldDy) {
        this.oldDy = oldDy;
    }

    public double getDx() {
        return dx;
    }

    public void setDx(double dx) {
        this.dx = dx;
    }

    public double getDy() {
        return dy;
    }

    public void setDy(double dy) {
        this.dy = dy;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}