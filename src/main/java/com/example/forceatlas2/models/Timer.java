package com.example.forceatlas2.models;

import java.time.Duration;
import java.time.LocalDateTime;

public class Timer {

    public String name;
    public LocalDateTime startTime;
    public LocalDateTime totalTime;

    public void start() {
        this.startTime = LocalDateTime.now();
    }

    public void stop() {
        this.totalTime = LocalDateTime.now();
        Long diff = Duration.between(totalTime, startTime).getSeconds();
    }

    public String display() {
        String info = this.name + " took " + this.totalTime + " seconds";
        return info;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timer(String name) {
        this.name = name;
    }
}
