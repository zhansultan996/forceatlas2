package com.example.forceatlas2;

import com.example.forceatlas2.models.Edge;
import com.example.forceatlas2.models.ForceAtlas2;
import com.example.forceatlas2.models.responses.NetworkxLayoutResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Forceatlas2Application {

	public static void main(String[] args) {

		SpringApplication.run(Forceatlas2Application.class, args);

	}

}
