package com.example.forceatlas2.exceptions;

import com.example.forceatlas2.configs.ApplicationPropertiesLoader;
import com.example.forceatlas2.configs.SpringBeanUtil;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import net.logstash.logback.stacktrace.StackElementFilter;
import net.logstash.logback.stacktrace.StackHasher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Deque;
import java.util.Locale;
import java.util.UUID;

/**
 * Родительский класс для специфичных ошибок AV, которые могут выстрелить в апи.
 * Апи должно кидать ошибки ТОЛЬКО этого родителя и никакие другие.
 * <p>
 * У ошибки есть понятие "Фатальности". Это значит - стоит ли повторять запрос еще раз через какое то время или нет.
 * За это понятие отвечает поле "fatal".
 * <p>
 * Если fatal == true, то повторять запрос нет смысла до тех пор,
 * пока техническая поддержка не решит этот вопрос. Если повторять запрос до исправления его технической поддержко,
 * то этот запрос всегда будет возвращать одну и ту же ошибку.
 * <p>
 * Если fatal == false, то значит можно повторить запрос через какой то промежуток времени и он может быть успешным на этот раз.
 */
@Getter
public abstract class GraphException extends Exception {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphException.class);

    @Expose // какие поля будут сериализовываться при Gson.toJson();
    private String application;
    @Expose
    private final String httpStatus;
    @Expose
    private final String userMessage;
    @Expose
    private final String developerMessage;
    @Expose
    private final String timestamp;
    @Expose
    private final String stackHash;
    @Expose
    private final String guid;
    @Expose
    private final String exceptionName;

    protected GraphException(HttpStatus httpStatus, String userMessagePropertyKey,
                             String developerMessagePropertyKey, String userMessage, String developerMessage) {
        try {
            this.application = ApplicationPropertiesLoader.loadProperties().getProperty("micro.service.name");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("THIS IS THE END OF MY LIFE!!! GOOD BYE EVERYONE!!!");
            System.exit(0);
        }

        ResourceBundleMessageSource resourceBundleMessageSource = SpringBeanUtil.getBean(ResourceBundleMessageSource.class);

        this.timestamp = LocalDateTime.now().toString();
        this.stackHash = stackHash(this);
        this.guid = UUID.randomUUID().toString();
        this.exceptionName = this.getClass().getCanonicalName();
        this.httpStatus = String.valueOf(httpStatus.value());
        this.userMessage = resourceBundleMessageSource.getMessage(userMessagePropertyKey, new Object[]{userMessage}, SpringBeanUtil.getLocale());
        this.developerMessage = resourceBundleMessageSource.getMessage(developerMessagePropertyKey, null, Locale.ENGLISH) + " ### " + developerMessage;

        LOGGER.error("MSG FROM EXCEPTION CONSTRUCTOR: " + this.developerMessage);
    }

    protected GraphException(HttpStatus httpStatus, String userMessagePropertyKey,
                             String developerMessagePropertyKey, String userMessage, String developerMessage, Throwable cause) {
        try {
            this.application = ApplicationPropertiesLoader.loadProperties().getProperty("micro.service.name");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("THIS IS THE END OF MY LIFE!!! GOOD BYE EVERYONE!!!");
            System.exit(0);
        }

        ResourceBundleMessageSource resourceBundleMessageSource = SpringBeanUtil.getBean(ResourceBundleMessageSource.class);

        this.initCause(cause);
        this.timestamp = LocalDateTime.now().toString();
        this.stackHash = stackHash(this);
        this.guid = UUID.randomUUID().toString();
        this.exceptionName = this.getClass().getCanonicalName();
        this.httpStatus = String.valueOf(httpStatus.value());
        this.userMessage = resourceBundleMessageSource.getMessage(userMessagePropertyKey, new Object[]{userMessage}, SpringBeanUtil.getLocale());
        this.developerMessage = resourceBundleMessageSource.getMessage(developerMessagePropertyKey, null, Locale.ENGLISH) + " ### " + developerMessage;

        LOGGER.error("MSG FROM EXCEPTION CONSTRUCTOR: " + this.developerMessage);
    }

    private String stackHash(Throwable error) {

        final StackHasher stackHasher = new StackHasher(new StackElementFilter() {
            @Override
            public boolean accept(final StackTraceElement stackTraceElement) {
                return stackTraceElement.isNativeMethod();
            }
        });

        final Deque<String> hash = stackHasher.hexHashes(error);
        return String.join("", hash);
    }
}


